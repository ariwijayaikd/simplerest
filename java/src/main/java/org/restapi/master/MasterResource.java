package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/master")
@Tag(name = "Modul Master")
public class MasterResource {

    @Inject
    MySQLPool pool;

    @GET
    @Path("/agama")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "getAgama",
            description = "endpoint ini akan menampilkan daftar agama"
    )
    public Multi<Agama> getAgama() {
        return Agama.findAgama(pool);
    }

    @GET
    @Path("/jalur-masuk")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "getJalurMasuk",
            description = "endpoint ini akan menampilkan daftar jalur masuk"
    )
    public Multi<JalurMasuk> getJalurMasuk(@QueryParam("jalur") String jalur_masuk) {
        if (jalur_masuk == null) {
            return JalurMasuk.findJalurMasuk(pool);
        } else {
            return JalurMasuk.findJalurMasukByQuery(pool, jalur_masuk);
        }
    }

    @GET
    @Path("/jenjang-pendidikan")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "getJenjangPendidikan",
            description = "endpoint ini akan menampilkan daftar jenjang pendidikan"
    )
    public Multi<JenjangPendidikan> getJenjangPendidikan(@QueryParam("nama") String nama_jenjang_pendidikan) {
        if (nama_jenjang_pendidikan == null) {
            return JenjangPendidikan.findJenjangPendidikan(pool);
        } else {
            return JenjangPendidikan.findJenjangPendidikanByQuery(pool, nama_jenjang_pendidikan);
        }
    }

    @GET
    @Path("/negara")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "getNegara",
            description = "endpoint ini akan menampilkan daftar negara"
    )
    public Multi<Negara> getNegara(@QueryParam("nama") String nama) {
        if (nama == null) {
            return Negara.findNegara(pool);
        } else {
            return Negara.findNegaraByQuery(pool, nama);
        }
    }

    @GET
    @Path("/tahun-ajar")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "getTahunAjar",
            description = "endpoint ini akan menampilkan daftar tahun ajar"
    )
    public Multi<TahunAjar> getTahunAjar(@QueryParam("nama") String nama_tahun_ajar) {
        if (nama_tahun_ajar == null) {
            return TahunAjar.findTahunAjar(pool);
        } else {
            return TahunAjar.findTahunAjarByQuery(pool, nama_tahun_ajar);
        }
    }

    @GET
    @Path("/status-mahasiswa")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "getStatusMahasiswa",
            description = "endpoint ini akan menampilkan daftar status mahasiswa"
    )
    public Multi<StatusMahasiswa> getStatusMahasiswa(@QueryParam("nama") String nama) {
        if (nama == null) {
            return StatusMahasiswa.findStatusMahasiswa(pool);
        } else {
            return StatusMahasiswa.findStatusMahasiswaByQuery(pool, nama);
        }
    }
}