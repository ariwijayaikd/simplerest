package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;

public class Negara {
    public Integer id;
    public String country_code;
    public String nama;
    public String nama_en;

    public Negara(
            Integer id,
            String country_code,
            String nama,
            String nama_en
    ) {
        this.id = id;
        this.country_code = country_code;
        this.nama = nama;
        this.nama_en = nama_en;
    }

    private static Negara from(Row row){
        return new Negara(
                row.getInteger("id"),
                row.getString("country_code"),
                row.getString("nama"),
                row.getString("nama_en")
        );
    }

    public static Multi<Negara> findNegara(MySQLPool pool) {
        return pool.query("SELECT id," +
                        "country_code," +
                        "nama," +
                        "nama_en " +
                        "FROM m_negara").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(Negara::from);
    }

    public static Multi<Negara> findNegaraByQuery(MySQLPool pool, String nama) {
        String negara = "'%"+ nama +"%'";
        return pool.query("SELECT id," +
                        "country_code," +
                        "nama," +
                        "nama_en " +
                        "FROM m_negara " +
                        "WHERE nama LIKE "+negara).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(Negara::from);
    }
}
