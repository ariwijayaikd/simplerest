package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;

public class JenjangPendidikan {
    public Integer id;
    public String nama_jenjang_studi;
    public String jenjang_pendidikan_lanjutan;
    public String kkni;
    public String persyaratan_penerimaan;
    public String jenis_pendidikan;
    public String jenis_jenjang;
    public Integer urut;
    public Integer lama_studi_tepat_waktu;
    public Integer max_lama_studi;
    public Integer max_lama;
    public Integer max_jenjang_bst;
    public Integer min_sks_lulus;

    public JenjangPendidikan(
            Integer id,
            String nama_jenjang_studi,
            String jenjang_pendidikan_lanjutan,
            String kkni,
            String persyaratan_penerimaan,
            String jenis_pendidikan,
            String jenis_jenjang,
            Integer urut,
            Integer lama_studi_tepat_waktu,
            Integer max_lama_studi,
            Integer max_lama,
            Integer max_jenjang_bst,
            Integer min_sks_lulus
    ) {
        this.id = id;
        this.nama_jenjang_studi = nama_jenjang_studi;
        this.jenjang_pendidikan_lanjutan = jenjang_pendidikan_lanjutan;
        this.kkni = kkni;
        this.persyaratan_penerimaan = persyaratan_penerimaan;
        this.jenis_pendidikan = jenis_pendidikan;
        this.jenis_jenjang = jenis_jenjang;
        this.urut = urut;
        this.lama_studi_tepat_waktu = lama_studi_tepat_waktu;
        this.max_lama_studi = max_lama_studi;
        this.max_lama = max_lama;
        this.max_jenjang_bst = max_jenjang_bst;
        this.min_sks_lulus = min_sks_lulus;
    }

    private static JenjangPendidikan from(Row row){
        return new JenjangPendidikan(
                row.getInteger("id"),
                row.getString("nama_jenjang_studi"),
                row.getString("jenjang_pendidikan_lanjutan"),
                row.getString("kkni"),
                row.getString("persyaratan_penerimaan"),
                row.getString("jenis_pendidikan"),
                row.getString("jenis_jenjang"),
                row.getInteger("urut"),
                row.getInteger("lama_studi_tepat_waktu"),
                row.getInteger("max_lama_studi"),
                row.getInteger("max_lama"),
                row.getInteger("max_jenjang_bst"),
                row.getInteger("min_sks_lulus")
        );
    }

    public static Multi<JenjangPendidikan> findJenjangPendidikan(MySQLPool pool) {
        return pool.query("SELECT id," +
                        "nama_jenjang_studi," +
                        "jenjang_pendidikan_lanjutan," +
                        "kkni," +
                        "persyaratan_penerimaan," +
                        "jenis_pendidikan," +
                        "jenis_jenjang," +
                        "urut," +
                        "lama_studi_tepat_waktu," +
                        "max_lama_studi," +
                        "max_lama," +
                        "max_jenjang_bst," +
                        "min_sks_lulus " +
                        "FROM m_jenjang_studi").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(JenjangPendidikan::from);
    }

    public static Multi<JenjangPendidikan> findJenjangPendidikanByQuery(MySQLPool pool, String nama_jenjang_studi) {
        String jenjang = "'%"+ nama_jenjang_studi +"%'";
        return pool.query("SELECT id," +
                        "nama_jenjang_studi," +
                        "jenjang_pendidikan_lanjutan," +
                        "kkni," +
                        "persyaratan_penerimaan," +
                        "jenis_pendidikan," +
                        "jenis_jenjang," +
                        "urut," +
                        "lama_studi_tepat_waktu," +
                        "max_lama_studi," +
                        "max_lama," +
                        "max_jenjang_bst," +
                        "min_sks_lulus " +
                        "FROM m_jenjang_studi " +
                        "WHERE nama_jenjang_studi LIKE "+jenjang).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(JenjangPendidikan::from);
    }
}
