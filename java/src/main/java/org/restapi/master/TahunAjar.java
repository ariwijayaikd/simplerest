package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;

import java.time.LocalDate;

public class TahunAjar {
    public Integer id;
    public String nama_tahun_ajar;
    public String nama_tahun_ajar_baru;
    public Integer tahun;
    public Integer semester;
    public LocalDate start_tahun_ajar;
    public LocalDate end_tahun_ajar;
    public String active;
    public String disable;
    public Integer id_tahun_ajar_old;
    public Integer active_oase;

    public TahunAjar(
            Integer id,
            String nama_tahun_ajar,
            String nama_tahun_ajar_baru,
            Integer tahun, Integer semester,
            LocalDate start_tahun_ajar,
            LocalDate end_tahun_ajar,
            String active,
            String disable,
            Integer id_tahun_ajar_old,
            Integer active_oase
    ) {
        this.id = id;
        this.nama_tahun_ajar = nama_tahun_ajar;
        this.nama_tahun_ajar_baru = nama_tahun_ajar_baru;
        this.tahun = tahun;
        this.semester = semester;
        this.start_tahun_ajar = start_tahun_ajar;
        this.end_tahun_ajar = end_tahun_ajar;
        this.active = active;
        this.disable = disable;
        this.id_tahun_ajar_old = id_tahun_ajar_old;
        this.active_oase = active_oase;
    }

    private static TahunAjar from(Row row){
        return new TahunAjar(
                row.getInteger("id"),
                row.getString("nama_tahun_ajar"),
                row.getString("nama_tahun_ajar_baru"),
                row.getInteger("tahun"),
                row.getInteger("semester"),
                row.getLocalDate("start_tahun_ajar"),
                row.getLocalDate("end_tahun_ajar"),
                row.getString("active"),
                row.getString("disable"),
                row.getInteger("id_tahun_ajar_old"),
                row.getInteger("active_oase")
        );
    }

    public static Multi<TahunAjar> findTahunAjar(MySQLPool pool) {
        return pool.query("SELECT id," +
                        "nama_tahun_ajar," +
                        "nama_tahun_ajar_baru," +
                        "tahun," +
                        "semester," +
                        "start_tahun_ajar," +
                        "end_tahun_ajar," +
                        "active," +
                        "disable," +
                        "id_tahun_ajar_old," +
                        "active_oase " +
                        "FROM m_tahun_ajar " +
                        "ORDER BY id DESC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(TahunAjar::from);
    }

    public static Multi<TahunAjar> findTahunAjarByQuery(MySQLPool pool, String nama_tahun_ajar) {
        String tahunajar = "'%"+ nama_tahun_ajar +"%'";
        return pool.query("SELECT id," +
                        "nama_tahun_ajar," +
                        "nama_tahun_ajar_baru," +
                        "tahun," +
                        "semester," +
                        "start_tahun_ajar," +
                        "end_tahun_ajar," +
                        "active," +
                        "disable," +
                        "id_tahun_ajar_old," +
                        "active_oase " +
                        "FROM m_tahun_ajar " +
                        "WHERE nama_tahun_ajar LIKE "+ tahunajar +
                        " ORDER BY id DESC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(TahunAjar::from);
    }
}
