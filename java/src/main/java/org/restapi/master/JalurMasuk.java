package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;

public class JalurMasuk {
    public Integer id;
    public String jalur_masuk;

    public JalurMasuk(
            Integer id,
            String jalur_masuk
    ) {
        this.id = id;
        this.jalur_masuk = jalur_masuk;
    }

    private static JalurMasuk from(Row row){
        return new JalurMasuk(
                row.getInteger("id"),
                row.getString("jalur_masuk")
        );
    }

    public static Multi<JalurMasuk> findJalurMasuk(MySQLPool pool) {
        return pool.query("SELECT id, jalur_masuk FROM m_jalur_masuk").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(JalurMasuk::from);
    }

    public static Multi<JalurMasuk> findJalurMasukByQuery(MySQLPool pool, String jalur_masuk) {
        String jalur = "'%" + jalur_masuk + "%'";
        return pool.query("SELECT " +
                        "id, " +
                        "jalur_masuk " +
                        "FROM m_jalur_masuk " +
                        "WHERE jalur_masuk LIKE "+jalur).execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(JalurMasuk::from);
    }
}
