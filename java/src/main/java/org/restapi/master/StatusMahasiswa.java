package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;

public class StatusMahasiswa {
    public Integer id;
    public String nama;
    public String status_id;
    public Integer status;
    public Integer parent;
    public Integer has_child;
    public String label_color;
    public Integer old_id;
    public Integer perlu_valid;
    public String show_data;
    public Integer show_rekon;

    public StatusMahasiswa(
            Integer id,
            String nama,
            String status_id,
            Integer status,
            Integer parent,
            Integer has_child,
            String label_color,
            Integer old_id,
            Integer perlu_valid,
            String show_data,
            Integer show_rekon
    ) {
        this.id = id;
        this.nama = nama;
        this.status_id = status_id;
        this.status = status;
        this.parent = parent;
        this.has_child = has_child;
        this.label_color = label_color;
        this.old_id = old_id;
        this.perlu_valid = perlu_valid;
        this.show_data = show_data;
        this.show_rekon = show_rekon;
    }

    private static StatusMahasiswa from(Row row){
        return new StatusMahasiswa(
                row.getInteger("id"),
                row.getString("nama"),
                row.getString("status_id"),
                row.getInteger("status"),
                row.getInteger("parent"),
                row.getInteger("has_child"),
                row.getString("label_color"),
                row.getInteger("old_id"),
                row.getInteger("perlu_valid"),
                row.getString("show_data"),
                row.getInteger("show_rekon")
        );
    }

    public static Multi<StatusMahasiswa> findStatusMahasiswa(MySQLPool pool) {
        return pool.query("SELECT id," +
                        "nama," +
                        "status_id," +
                        "status, parent," +
                        "has_child," +
                        "label_color," +
                        "old_id," +
                        "perlu_valid," +
                        "show_data," +
                        "show_rekon " +
                        "FROM m_status_mahasiswa " +
                        "ORDER BY id ASC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(StatusMahasiswa::from);
    }

    public static Multi<StatusMahasiswa> findStatusMahasiswaByQuery(MySQLPool pool, String nama) {
        String status = "'%"+ nama +"%'";
        return pool.query("SELECT id," +
                        "nama," +
                        "status_id," +
                        "status, parent," +
                        "has_child," +
                        "label_color," +
                        "old_id," +
                        "perlu_valid," +
                        "show_data," +
                        "show_rekon " +
                        "FROM m_status_mahasiswa " +
                        "WHERE nama LIKE "+ status +
                        " ORDER BY id ASC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(StatusMahasiswa::from);
    }
}