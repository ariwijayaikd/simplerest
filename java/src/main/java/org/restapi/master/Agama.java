package org.restapi.master;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "getAgama")
public class Agama {
    public Integer id;
    public String nama;

    public Agama(
            Integer id,
            String nama
    ) {
        this.id = id;
        this.nama = nama;
    }

    private static Agama from(Row row) {
        return new Agama(
                row.getInteger("id"),
                row.getString("nama"));
    }

    public static Multi<Agama> findAgama(MySQLPool pool) {
        return pool.query("SELECT id, nama FROM m_agama").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(Agama::from);
    }
}