package org.restapi.krs;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/krs/mahasiswa/")
@Tag(name = "Modul KRS", description = "Modul API ini digunakan untuk melihat data KRS mahasiswa")
@Produces(MediaType.APPLICATION_JSON)
public class KrsResource {
    @Inject
    MySQLPool pool;

    @GET
    @Path("/{id_mahasiswa}")
    @Operation(
            summary = "getListKrsMahasiswa",
            description = "endpoint ini akan menampilkan data ringkas krs mahasiswa"
    )
    public Multi<ListKrsMahasiswa> getListKrsMahasiswa(@PathParam("id_mahasiswa") Integer id_mahasiswa) {
        return ListKrsMahasiswa.findKrsMahasiswa(pool, id_mahasiswa);
    }

    @GET
    @Path("/{id_mahasiswa}/tahun-ajar/{id_tahun_ajar}")
    @Operation(
            summary = "getKrsMahasiswa",
            description = "endpoint ini akan menampilkan data krs mahasiswa"
    )
    public Multi<KrsMahasiswa> getKrsMahasiswa(@PathParam("id_mahasiswa") Integer id_mahasiswa, @PathParam("id_tahun_ajar") Integer id_tahun_ajar) {
        return KrsMahasiswa.findKrsMahasiswaFilterByTahunAjar(pool, id_mahasiswa, id_tahun_ajar);
    }
}
