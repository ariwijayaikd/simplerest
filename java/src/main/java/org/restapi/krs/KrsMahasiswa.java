package org.restapi.krs;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;

@Schema(name = "getKrsMahasiswa")
public class KrsMahasiswa {
    public Integer id;
    @Schema(required = true, example = "199216")
    public Integer id_mahasiswa;
    public String nama_mahasiswa;
    public Integer id_matakuliah;
    public String nama_matakuliah;
    public Integer id_tahun_ajar;
    public String nama_tahun_ajar;
    public Integer merdeka_belajar_id;
    public Integer id_pa;
    public Integer ips;
    public LocalDate tgl_krs;
    public LocalDate tgl_approve;
    public Integer status_approve;
    public Integer id_jurusan;
    public String nama_jurusan;
    public Integer status_mk_krs;
    public String nama_status_mk;
    public Integer jumlah_mk;
    public Integer jenis_krs;
    public String nama_jenis_krs;
    public Integer is_titip_siklus;
    public String id_dokumen;
    public String file_krs_sign;

    public KrsMahasiswa(
            Integer id,
            Integer id_mahasiswa,
            String nama_mahasiswa,
            Integer id_matakuliah,
            String nama_matakuliah,
            Integer id_tahun_ajar,
            String nama_tahun_ajar,
            Integer merdeka_belajar_id,
            Integer id_pa,
            Integer ips,
            LocalDate tgl_krs,
            LocalDate tgl_approve,
            Integer status_approve,
            Integer id_jurusan,
            String nama_jurusan,
            Integer status_mk_krs,
            String nama_status_mk,
            Integer jumlah_mk,
            Integer jenis_krs,
            String nama_jenis_krs,
            Integer is_titip_siklus,
            String id_dokumen,
            String file_krs_sign
    ) {
        this.id = id;
        this.id_mahasiswa = id_mahasiswa;
        this.nama_mahasiswa = nama_mahasiswa;
        this.id_matakuliah = id_matakuliah;
        this.nama_matakuliah = nama_matakuliah;
        this.id_tahun_ajar = id_tahun_ajar;
        this.nama_tahun_ajar = nama_tahun_ajar;
        this.merdeka_belajar_id = merdeka_belajar_id;
        this.id_pa = id_pa;
        this.ips = ips;
        this.tgl_krs = tgl_krs;
        this.tgl_approve = tgl_approve;
        this.status_approve = status_approve;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.status_mk_krs = status_mk_krs;
        this.nama_status_mk = nama_status_mk;
        this.jumlah_mk = jumlah_mk;
        this.jenis_krs = jenis_krs;
        this.nama_jenis_krs = nama_jenis_krs;
        this.is_titip_siklus = is_titip_siklus;
        this.id_dokumen = id_dokumen;
        this.file_krs_sign = file_krs_sign;
    }

    private static KrsMahasiswa from(Row row) {
        return new KrsMahasiswa(row.getInteger("id"),
                row.getInteger("id_mahasiswa"),
                row.getString("nama_mahasiswa"),
                row.getInteger("id_matakuliah"),
                row.getString("nama_matakuliah"),
                row.getInteger("id_tahun_ajar"),
                row.getString("nama_tahun_ajar"),
                row.getInteger("merdeka_belajar_id"),
                row.getInteger("id_pa"),
                row.getInteger("ips"),
                row.getLocalDate("tgl_krs"),
                row.getLocalDate("tgl_approve"),
                row.getInteger("status_approve"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getInteger("status_mk_krs"),
                row.getString("nama_status_mk"),
                row.getInteger("jumlah_mk"),
                row.getInteger("jenis_krs"),
                row.getString("nama_jenis_krs"),
                row.getInteger("is_titip_siklus"),
                row.getString("id_dokumen"),
                row.getString("file_krs_sign"));
    }

    public static Multi<KrsMahasiswa> findKrsMahasiswaFilterByTahunAjar(MySQLPool pool, Integer id_mahasiswa, Integer id_tahun_ajar) {
        return pool.preparedQuery("SELECT t_krs.id, " +
                        "t_krs.id_mahasiswa, " +
                        "m_mahasiswa.nama as nama_mahasiswa, " +
                        "t_krs_detail.id_matakuliah, " +
                        "m_matakuliah.nama_matakuliah, " +
                        "t_krs.id_tahun_ajar, " +
                        "m_tahun_ajar.nama_tahun_ajar, " +
                        "t_krs.merdeka_belajar_id, " +
                        "t_krs.id_pa, " +
                        "t_krs.ips, " +
                        "t_krs.tgl_krs, " +
                        "t_krs.tgl_approve, " +
                        "t_krs.status_approve, " +
                        "t_krs.id_jurusan, " +
                        "m_jurusan.nama_jurusan, " +
                        "t_krs.status_mk_krs, " +
                        "m_status_matakuliah.nama as nama_status_mk, " +
                        "t_krs.jumlah_mk, " +
                        "t_krs.jenis_krs, " +
                        "m_jenis_matakuliah.nama as nama_jenis_krs," +
                        "t_krs.is_titip_siklus, " +
                        "t_krs.id_dokumen, " +
                        "t_krs.file_krs_sign " +
                        "FROM " +
                        "t_krs " +
                        "LEFT JOIN t_krs_detail ON " +
                        "t_krs.id = t_krs_detail.id_krs  " +
                        "LEFT JOIN m_matakuliah ON " +
                        "t_krs_detail.id_matakuliah = m_matakuliah.id " +
                        "LEFT JOIN m_mahasiswa ON " +
                        "t_krs.id_mahasiswa = m_mahasiswa.id " +
                        "LEFT JOIN m_tahun_ajar ON " +
                        "t_krs.id_tahun_ajar = m_tahun_ajar.id " +
                        "LEFT JOIN m_jurusan ON " +
                        "t_krs.id_jurusan  = m_jurusan.id " +
                        "LEFT JOIN m_status_matakuliah ON " +
                        "t_krs.status_mk_krs = m_status_matakuliah.id " +
                        "LEFT JOIN m_jenis_matakuliah ON " +
                        "t_krs.jenis_krs = m_jenis_matakuliah.id " +
                        "WHERE " +
                        "id_mahasiswa LIKE ? " +
                        "AND id_tahun_ajar LIKE ?").execute(Tuple.of(id_mahasiswa, id_tahun_ajar))
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(KrsMahasiswa::from);
    }
}
