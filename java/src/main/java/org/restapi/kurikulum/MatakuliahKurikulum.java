package org.restapi.kurikulum;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import java.time.LocalDate;

@Schema(name = "getMatakuliahKurikulum")
public class MatakuliahKurikulum {

    @Schema(required = true, description = "data berupa id dari matakuliah", example = "10420")
    public Long id;
    public Integer id_jurusan;
    public String nama_jurusan;
    public Integer id_kurikulum;
    public String nama_kurikulum;
    public String kode_matakuliah;
    public Integer sks;
    public Integer semester;
    public String nama_matakuliah;
    public String nama_matakuliah_english;
    public Integer id_jenis_matakuliah;
    public String nama_jenis_matakuliah;
    public Integer id_status_matakuliah;
    public String nama_status_matakuliah;
    public String deskripsi_kurikulum;
    public LocalDate tmt_berlaku;
    public Integer id_tahun_ajar_mulai;
    public String nama_tahun_ajar_mulai;
    public String file_kurikulum;
    public Integer sks_lulus;
    public Integer status_kurikulum;
    public String nama_status_kurikulum;

    public MatakuliahKurikulum(Long id, Integer id_jurusan, String nama_jurusan, Integer id_kurikulum, String nama_kurikulum, String kode_matakuliah, Integer sks, Integer semester, String nama_matakuliah, String nama_matakuliah_english, Integer id_jenis_matakuliah, String nama_jenis_matakuliah, Integer id_status_matakuliah, String nama_status_matakuliah, String deskripsi_kurikulum, LocalDate tmt_berlaku, Integer id_tahun_ajar_mulai, String nama_tahun_ajar_mulai, String file_kurikulum, Integer sks_lulus, Integer status_kurikulum, String nama_status_kurikulum) {
        this.id = id;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.id_kurikulum = id_kurikulum;
        this.nama_kurikulum = nama_kurikulum;
        this.kode_matakuliah = kode_matakuliah;
        this.sks = sks;
        this.semester = semester;
        this.nama_matakuliah = nama_matakuliah;
        this.nama_matakuliah_english = nama_matakuliah_english;
        this.id_jenis_matakuliah = id_jenis_matakuliah;
        this.nama_jenis_matakuliah = nama_jenis_matakuliah;
        this.id_status_matakuliah = id_status_matakuliah;
        this.nama_status_matakuliah = nama_status_matakuliah;
        this.deskripsi_kurikulum = deskripsi_kurikulum;
        this.tmt_berlaku = tmt_berlaku;
        this.id_tahun_ajar_mulai = id_tahun_ajar_mulai;
        this.nama_tahun_ajar_mulai = nama_tahun_ajar_mulai;
        this.file_kurikulum = file_kurikulum;
        this.sks_lulus = sks_lulus;
        this.status_kurikulum = status_kurikulum;
        this.nama_status_kurikulum = nama_status_kurikulum;
    }

    private static MatakuliahKurikulum from(Row row) {
        return new MatakuliahKurikulum(
                row.getLong("id"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getInteger("id_kurikulum"),
                row.getString("nama_kurikulum"),
                row.getString("kode_matakuliah"),
                row.getInteger("sks"),
                row.getInteger("semester"),
                row.getString("nama_matakuliah"),
                row.getString("nama_matakuliah_english"),
                row.getInteger("id_jenis_matakuliah"),
                row.getString("nama_jenis_matakuliah"),
                row.getInteger("id_status_matakuliah"),
                row.getString("nama_status_matakuliah"),
                row.getString("deskripsi_kurikulum"),
                row.getLocalDate("tmt_berlaku"),
                row.getInteger("id_tahun_ajar_mulai"),
                row.getString("nama_tahun_ajar_mulai"),
                row.getString("file_kurikulum"),
                row.getInteger("sks_lulus"),
                row.getInteger("status_kurikulum"),
                row.getString("nama_status_kurikulum"));
    }

    public static Multi<MatakuliahKurikulum> findMatakuliahKurikulumByIdKurikulum(MySQLPool pool, Integer id_kurikulum) {
        return pool.preparedQuery("SELECT m_kurikulum.id," +
                        "m_kurikulum.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_matakuliah.id_kurikulum," +
                        "m_kurikulum.nama_kurikulum," +
                        "m_matakuliah.kode_matakuliah," +
                        "m_matakuliah.sks," +
                        "m_matakuliah.semester," +
                        "m_matakuliah.nama_matakuliah," +
                        "m_matakuliah.nama_matakuliah_english," +
                        "m_matakuliah.id_jenis_matakuliah," +
                        "m_jenis_matakuliah.nama as nama_jenis_matakuliah," +
                        "m_matakuliah.id_status_matakuliah," +
                        "m_status_matakuliah.nama as nama_status_matakuliah," +
                        "m_kurikulum.deskripsi_kurikulum," +
                        "m_kurikulum.tmt_berlaku," +
                        "m_kurikulum.id_tahun_ajar_mulai," +
                        "m_tahun_ajar.nama_tahun_ajar as nama_tahun_ajar_mulai," +
                        "m_kurikulum.file_kurikulum," +
                        "m_kurikulum.sks_lulus," +
                        "m_kurikulum.status_kurikulum," +
                        "m_status_kurikulum.nama as nama_status_kurikulum " +
                        "FROM m_kurikulum " +
                        "LEFT JOIN m_jurusan ON m_kurikulum.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_tahun_ajar ON m_kurikulum.id_tahun_ajar_mulai=m_tahun_ajar.id AND m_kurikulum.id_tahun_ajar_mulai=m_tahun_ajar.id " +
                        "LEFT JOIN m_status_kurikulum ON m_kurikulum.status_kurikulum=m_status_kurikulum.id " +
                        "LEFT JOIN m_matakuliah ON m_kurikulum.id=m_matakuliah.id_kurikulum " +
                        "LEFT JOIN m_jenis_matakuliah ON m_matakuliah.id_jenis_matakuliah=m_jenis_matakuliah.id " +
                        "LEFT JOIN m_status_matakuliah ON m_matakuliah.id_status_matakuliah=m_status_matakuliah.id " +
                        "WHERE m_kurikulum.id LIKE ?").execute(Tuple.of(id_kurikulum))
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(MatakuliahKurikulum::from);
    }
}
