package org.restapi.kurikulum;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.time.LocalDate;

@Schema(name = "getListKurikulum")
public class ListKurikulum {
    @Schema(required = true, description = "data berupa id pada tabel m_kurikulum")
    public Integer id;
    @Schema(description = "data berupa id_jurusan pada tabel m_kurikulum")
    public Integer id_jurusan;
    @Schema(description = "data berupa nama_kurikulum pada tabel m_kurikulum")
    public String nama_jurusan;
    public String nama_kurikulum;
    public String deskripsi_kurikulum;
    public LocalDate tmt_berlaku;
    public Integer id_tahun_ajar_mulai;
    public String nama_tahun_ajar;
    public String file_kurikulum;
    public Integer sks_lulus;
    public Integer status_kurikulum;
    public String nama_status_kurikulum;


    public ListKurikulum(
            Integer id,
            Integer id_jurusan,
            String nama_jurusan,
            String nama_kurikulum,
            String deskripsi_kurikulum,
            LocalDate tmt_berlaku,
            Integer id_tahun_ajar_mulai,
            String nama_tahun_ajar,
            String file_kurikulum,
            Integer sks_lulus,
            Integer status_kurikulum,
            String nama_status_kurikulum
    ){
    this.id = id;
    this.id_jurusan = id_jurusan;
    this.nama_jurusan = nama_jurusan;
    this.nama_kurikulum = nama_kurikulum;
    this.deskripsi_kurikulum = deskripsi_kurikulum;
    this.tmt_berlaku = tmt_berlaku;
    this.id_tahun_ajar_mulai = id_tahun_ajar_mulai;
    this.nama_tahun_ajar = nama_tahun_ajar;
    this.file_kurikulum = file_kurikulum;
    this.sks_lulus = sks_lulus;
    this.status_kurikulum = status_kurikulum;
    this.nama_status_kurikulum = nama_status_kurikulum;
    }

    private static ListKurikulum from(Row row) {
        return new ListKurikulum(
                row.getInteger("id"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getString("nama_kurikulum"),
                row.getString("deskripsi_kurikulum"),
                row.getLocalDate("tmt_berlaku"),
                row.getInteger("id_tahun_ajar_mulai"),
                row.getString("nama_tahun_ajar"),
                row.getString("file_kurikulum"),
                row.getInteger("sks_lulus"),
                row.getInteger("status_kurikulum"),
                row.getString("nama_status_kurikulum"));
    }

    public static Multi<ListKurikulum> findAllKurikulum(MySQLPool pool) {
        return pool.query("SELECT m_kurikulum.id," +
                        "m_jurusan.id as id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_kurikulum.nama_kurikulum," +
                        "m_kurikulum.deskripsi_kurikulum," +
                        "m_kurikulum.tmt_berlaku," +
                        "m_kurikulum.id_tahun_ajar_mulai," +
                        "m_tahun_ajar.nama_tahun_ajar," +
                        "m_kurikulum.file_kurikulum," +
                        "m_kurikulum.sks_lulus," +
                        "m_kurikulum.status_kurikulum," +
                        "m_status_kurikulum.nama as nama_status_kurikulum " +
                        "FROM m_kurikulum " +
                        "LEFT JOIN m_jurusan ON m_kurikulum.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_tahun_ajar ON m_kurikulum.id_tahun_ajar_mulai = m_tahun_ajar.id " +
                        "LEFT JOIN m_status_kurikulum ON m_kurikulum.status_kurikulum = m_status_kurikulum.id " +
                        "ORDER BY m_kurikulum.id_tahun_ajar_mulai DESC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ListKurikulum::from);
    }

    public static Multi<ListKurikulum> findAllKurikulumByQuery(MySQLPool pool, String nama_kurikulum) {
        String kurikulum = "'%"+nama_kurikulum+"%'";
        return pool.query("SELECT m_kurikulum.id," +
                        "m_jurusan.id as id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_kurikulum.nama_kurikulum," +
                        "m_kurikulum.deskripsi_kurikulum," +
                        "m_kurikulum.tmt_berlaku," +
                        "m_kurikulum.id_tahun_ajar_mulai," +
                        "m_tahun_ajar.nama_tahun_ajar," +
                        "m_kurikulum.file_kurikulum," +
                        "m_kurikulum.sks_lulus," +
                        "m_kurikulum.status_kurikulum," +
                        "m_status_kurikulum.nama as nama_status_kurikulum " +
                        "FROM m_kurikulum " +
                        "LEFT JOIN m_jurusan ON m_kurikulum.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_tahun_ajar ON m_kurikulum.id_tahun_ajar_mulai = m_tahun_ajar.id " +
                        "LEFT JOIN m_status_kurikulum ON m_kurikulum.status_kurikulum = m_status_kurikulum.id " +
                        "WHERE nama_kurikulum LIKE "+kurikulum+
                        " ORDER BY m_kurikulum.id_tahun_ajar_mulai DESC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ListKurikulum::from);
    }
}
