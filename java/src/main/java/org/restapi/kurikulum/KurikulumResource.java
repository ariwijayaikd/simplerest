package org.restapi.kurikulum;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/kurikulum")
@Tag(name = "Modul Matakuliah", description = "Modul ini digunakan untuk mendapatkan data seputar matakuliah dan kurikulum")
@Produces(MediaType.APPLICATION_JSON)
public class KurikulumResource {

    @Inject
    MySQLPool pool;

    @GET
    @Operation(
            summary = "getListKurikulum",
            description = "endpoint ini akan menampilkan data ringkas seluruh kurikulum pada database"
    )
    public Multi<ListKurikulum> getListKurikulum(@QueryParam("nama") String nama_kurikulum) {
        if (nama_kurikulum == null) {
            return ListKurikulum.findAllKurikulum(pool);
        } else {
            return ListKurikulum.findAllKurikulumByQuery(pool, nama_kurikulum);
        }
    }

    @GET
    @Path("/{id_kurikulum}")
    @Operation(
            summary = "getDetailKurikulum",
            description = "endpoint ini akan menampilkan data lengkap terkait kurikulum terpilih"
    )
    public Uni<Response> getDetailKurikulum(@PathParam("id_kurikulum") Integer id) {
        return DetailKurikulum.findKurikulumById(pool, id)
                .onItem().transform(kurikulum -> kurikulum != null ? Response.ok(kurikulum) : Response.status(Response.Status.NOT_FOUND))
                .onItem().transform(Response.ResponseBuilder::build);
    }

    @GET
    @Path("/{id_kurikulum}/matakuliah")
    @Operation(
            summary = "getMatakuliahKurikulum",
            description = "endpoint ini akan menampilkan hubungan antara data matakuliah dengan data kurikulum berdasarkan id_kurikulum"
    )
    public Multi<MatakuliahKurikulum> getMatakuliahKurikulum(@PathParam("id_kurikulum") Integer id_kurikulum) {
        return MatakuliahKurikulum.findMatakuliahKurikulumByIdKurikulum(pool, id_kurikulum);
    }
}