package org.restapi.matakuliah;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "getDetailMatakuliah")
public class DetailMatakuliah {

    @Schema(required = true, description = "data berupa id matakuliah", example = "10420")
    public Long id;
    @Schema(required = true, description = "data berupa id_jurusan")
    public Integer id_jurusan;
    public String nama_jurusan;
    @Schema(description = "data berupa id_kurikulum")
    public Integer id_kurikulum;
    public String nama_kurikulum;
    @Schema(description = "data berupa kode_matakuliah")
    public String kode_matakuliah;
    @Schema(description = "data berupa bobot sks")
    public Integer sks;
    @Schema(description = "data berupa semester")
    public Integer semester;
    @Schema(description = "data berupa nama_matakuliah")
    public String nama_matakuliah;
    @Schema(description = "data berupa nama_matakuliah_english")
    public String nama_matakuliah_english;
    @Schema(description = "data berupa id_jenis_matakuliah")
    public Integer id_jenis_matakuliah;
    public String nama_jenis_matakuliah;
    @Schema(description = "data berupa id_status_matakuliah")
    public Integer id_status_matakuliah;
    public String nama_status_matakuliah;

    public DetailMatakuliah(Long id, Integer id_jurusan, String nama_jurusan, Integer id_kurikulum, String nama_kurikulum, String kode_matakuliah, Integer sks, Integer semester, String nama_matakuliah, String nama_matakuliah_english, Integer id_jenis_matakuliah, String nama_jenis_matakuliah, Integer id_status_matakuliah, String nama_status_matakuliah) {
        this.id = id;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.id_kurikulum = id_kurikulum;
        this.nama_kurikulum = nama_kurikulum;
        this.kode_matakuliah = kode_matakuliah;
        this.sks = sks;
        this.semester = semester;
        this.nama_matakuliah = nama_matakuliah;
        this.nama_matakuliah_english = nama_matakuliah_english;
        this.id_jenis_matakuliah = id_jenis_matakuliah;
        this.nama_jenis_matakuliah = nama_jenis_matakuliah;
        this.id_status_matakuliah = id_status_matakuliah;
        this.nama_status_matakuliah = nama_status_matakuliah;
    }

    private static DetailMatakuliah from(Row row) {
        return new DetailMatakuliah(
                row.getLong("id"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getInteger("id_kurikulum"),
                row.getString("nama_kurikulum"),
                row.getString("kode_matakuliah"),
                row.getInteger("sks"),
                row.getInteger("semester"),
                row.getString("nama_matakuliah"),
                row.getString("nama_matakuliah_english"),
                row.getInteger("id_jenis_matakuliah"),
                row.getString("nama_jenis_matakuliah"),
                row.getInteger("id_status_matakuliah"),
                row.getString("nama_status_matakuliah"));
    }

    public static Uni<DetailMatakuliah> findMatakuliahById(MySQLPool pool, Long id) {
        return pool.preparedQuery("SELECT m_matakuliah.id," +
                        "m_matakuliah.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_matakuliah.id_kurikulum," +
                        "m_kurikulum.nama_kurikulum," +
                        "m_matakuliah.kode_matakuliah," +
                        "m_matakuliah.sks," +
                        "m_matakuliah.semester," +
                        "m_matakuliah.nama_matakuliah," +
                        "m_matakuliah.nama_matakuliah_english," +
                        "m_matakuliah.id_jenis_matakuliah," +
                        "m_jenis_matakuliah.nama as nama_jenis_matakuliah," +
                        "m_matakuliah.id_status_matakuliah," +
                        "m_status_matakuliah.nama as nama_status_matakuliah " +
                        "FROM m_matakuliah " +
                        "LEFT JOIN m_jurusan ON m_matakuliah.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_kurikulum ON m_matakuliah.id_kurikulum = m_kurikulum.id " +
                        "LEFT JOIN m_jenis_matakuliah ON m_matakuliah.id_jenis_matakuliah = m_jenis_matakuliah.id " +
                        "LEFT JOIN m_status_matakuliah ON m_matakuliah.id_status_matakuliah = m_status_matakuliah.id " +
                        "WHERE m_matakuliah.id LIKE ?").execute(Tuple.of(id))
                .onItem().transform(RowSet::iterator)
                .onItem().transform(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }
}
