package org.restapi.matakuliah;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "getListMatakuliah")
public class ListMatakuliah {
    public Long id;
    public Integer id_jurusan;
    public String nama_jurusan;
    public Integer id_kurikulum;
    public String nama_kurikulum;
    public String kode_matakuliah;
    public Integer sks;
    public Integer semester;
    public String nama_matakuliah;
    public Integer id_jenis_matakuliah;
    public String nama_jenis_matakuliah;
    public Integer id_status_matakuliah;
    public String nama_status_matakuliah;

    public ListMatakuliah(
            Long id,
            Integer id_jurusan,
            String nama_jurusan,
            Integer id_kurikulum,
            String nama_kurikulum,
            String kode_matakuliah,
            Integer sks,
            Integer semester,
            String nama_matakuliah,
            Integer id_jenis_matakuliah,
            String nama_jenis_matakuliah,
            Integer id_status_matakuliah,
            String nama_status_matakuliah
    ) {
        this.id = id;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.id_kurikulum = id_kurikulum;
        this.nama_kurikulum = nama_kurikulum;
        this.kode_matakuliah = kode_matakuliah;
        this.sks = sks;
        this.semester = semester;
        this.nama_matakuliah = nama_matakuliah;
        this.id_jenis_matakuliah = id_jenis_matakuliah;
        this.nama_jenis_matakuliah = nama_jenis_matakuliah;
        this.id_status_matakuliah = id_status_matakuliah;
        this.nama_status_matakuliah = nama_status_matakuliah;
    }

    private static ListMatakuliah from(Row row) {
        return new ListMatakuliah(
                row.getLong("id"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getInteger("id_kurikulum"),
                row.getString("nama_kurikulum"),
                row.getString("kode_matakuliah"),
                row.getInteger("sks"),
                row.getInteger("semester"),
                row.getString("nama_matakuliah"),
                row.getInteger("id_jenis_matakuliah"),
                row.getString("nama_jenis_matakuliah"),
                row.getInteger("id_status_matakuliah"),
                row.getString("nama_status_matakuliah"));
    }

    public static Multi<ListMatakuliah> findAllMatakuliah(MySQLPool pool) {
        return pool.query("SELECT m_matakuliah.id," +
                        "m_matakuliah.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_matakuliah.id_kurikulum," +
                        "m_kurikulum.nama_kurikulum," +
                        "m_matakuliah.kode_matakuliah," +
                        "m_matakuliah.sks," +
                        "m_matakuliah.semester," +
                        "m_matakuliah.nama_matakuliah," +
                        "m_matakuliah.id_jenis_matakuliah," +
                        "m_jenis_matakuliah.nama as nama_jenis_matakuliah," +
                        "m_matakuliah.id_status_matakuliah," +
                        "m_status_matakuliah.nama as nama_status_matakuliah " +
                        "FROM m_matakuliah " +
                        "LEFT JOIN m_jurusan ON m_matakuliah.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_kurikulum ON m_matakuliah.id_kurikulum = m_kurikulum.id " +
                        "LEFT JOIN m_jenis_matakuliah ON m_matakuliah.id_jenis_matakuliah = m_jenis_matakuliah.id " +
                        "LEFT JOIN m_status_matakuliah ON m_matakuliah.id_status_matakuliah = m_status_matakuliah.id " +
                        "ORDER BY id ASC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ListMatakuliah::from);
    }

    public static Multi<ListMatakuliah> findMatakuliahByQueryParam(MySQLPool pool, String nama_matakuliah) {
        String matkul = "'%"+ nama_matakuliah +"%'";
        return pool.query("SELECT m_matakuliah.id," +
                        "m_matakuliah.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_matakuliah.id_kurikulum," +
                        "m_kurikulum.nama_kurikulum," +
                        "m_matakuliah.kode_matakuliah," +
                        "m_matakuliah.sks," +
                        "m_matakuliah.semester," +
                        "m_matakuliah.nama_matakuliah," +
                        "m_matakuliah.id_jenis_matakuliah," +
                        "m_jenis_matakuliah.nama as nama_jenis_matakuliah," +
                        "m_matakuliah.id_status_matakuliah," +
                        "m_status_matakuliah.nama as nama_status_matakuliah " +
                        "FROM m_matakuliah " +
                        "LEFT JOIN m_jurusan ON m_matakuliah.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_kurikulum ON m_matakuliah.id_kurikulum = m_kurikulum.id " +
                        "LEFT JOIN m_jenis_matakuliah ON m_matakuliah.id_jenis_matakuliah = m_jenis_matakuliah.id " +
                        "LEFT JOIN m_status_matakuliah ON m_matakuliah.id_status_matakuliah = m_status_matakuliah.id " +
                "WHERE m_matakuliah.nama_matakuliah LIKE "+matkul+
                        " ORDER BY id ASC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ListMatakuliah::from);
    }
}