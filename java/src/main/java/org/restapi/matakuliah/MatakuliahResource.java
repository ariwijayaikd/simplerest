package org.restapi.matakuliah;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/matakuliah")
@Tag(name = "Modul Matakuliah", description = "Modul ini digunakan untuk mendapatkan data seputar matakuliah dan kurikulum")
@Produces(MediaType.APPLICATION_JSON)
public class MatakuliahResource {
    @Inject
    MySQLPool pool;

    @GET
    @Operation(
            summary = "getListMatakuliah",
            description = "endpoint ini akan menampilkan data ringkas seluruh matakuliah pada database"
    )
    public Multi<ListMatakuliah> getListMatakuliah(@QueryParam("nama") String nama_matakuliah) {
        if (nama_matakuliah == null) {
            return ListMatakuliah.findAllMatakuliah(pool);
        } else {
            return ListMatakuliah.findMatakuliahByQueryParam(pool, nama_matakuliah);
        }
    }

    @GET
    @Path("/{id_matakuliah}")
    @Operation(
            summary = "getDetailMatakuliah",
            description = "endpoint ini akan menampilkan data lengkap terkait matakuliah terpilih"
    )
    public Uni<Response> getDetailMatkuliah(@PathParam("id_matakuliah") Long id) {
        return DetailMatakuliah.findMatakuliahById(pool, id)
                .onItem().transform(matakuliah -> matakuliah != null ? Response.ok(matakuliah) : Response.status(Response.Status.NO_CONTENT))
                .onItem().transform(Response.ResponseBuilder::build);
    }
}