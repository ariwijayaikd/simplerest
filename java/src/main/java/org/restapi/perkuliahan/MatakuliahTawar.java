package org.restapi.perkuliahan;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "getMatakuliahtawar")
public class MatakuliahTawar {

    public Integer id;
    public Integer id_matakuliah;
    public String nama_matakuliah;
    public Integer id_jurusan;
    public String nama_jurusan;
    public Integer id_tahun_ajar;
    public String nama_tahun_ajar;
    public Integer is_reguler;
    public String is_reguler_nama;
    public Integer merdeka_belajar_id;
    public String merdeka_belajar_nama;
    public Integer is_kelas_internasional;
    public Integer jumlah_kelas;

    public MatakuliahTawar(
            Integer id,
            Integer id_matakuliah,
            String nama_matakuliah,
            Integer id_jurusan,
            String nama_jurusan,
            Integer id_tahun_ajar,
            String nama_tahun_ajar,
            Integer is_reguler,
            String is_reguler_nama,
            Integer merdeka_belajar_id,
            String merdeka_belajar_nama,
            Integer is_kelas_internasional,
            Integer jumlah_kelas
    ) {
        this.id = id;
        this.id_matakuliah = id_matakuliah;
        this.nama_matakuliah = nama_matakuliah;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.id_tahun_ajar = id_tahun_ajar;
        this.nama_tahun_ajar = nama_tahun_ajar;
        this.is_reguler = is_reguler;
        this.is_reguler_nama = is_reguler_nama;
        this.merdeka_belajar_id = merdeka_belajar_id;
        this.merdeka_belajar_nama = merdeka_belajar_nama;
        this.is_kelas_internasional = is_kelas_internasional;
        this.jumlah_kelas = jumlah_kelas;
    }

    private static MatakuliahTawar from(Row row) {
        return new MatakuliahTawar(
                row.getInteger("id"),
                row.getInteger("id_matakuliah"),
                row.getString("nama_matakuliah"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getInteger("id_tahun_ajar"),
                row.getString("nama_tahun_ajar"),
                row.getInteger("is_reguler"),
                row.getString("is_reguler_nama"),
                row.getInteger("merdeka_belajar_id"),
                row.getString("merdeka_belajar_nama"),
                row.getInteger("is_kelas_internasional"),
                row.getInteger("jumlah_kelas"));
    }

    public static Multi<MatakuliahTawar> findMatakuliahTawar(MySQLPool pool, Integer id_jurusan, Integer id_tahun_ajar) {
        return pool.preparedQuery("SELECT t_matakuliah_tawar.id," +
                        "t_matakuliah_tawar.id_matakuliah," +
                        "m_matakuliah.nama_matakuliah," +
                        "t_matakuliah_tawar.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "t_matakuliah_tawar.id_tahun_ajar," +
                        "m_tahun_ajar.nama_tahun_ajar," +
                        "t_matakuliah_tawar.is_reguler," +
                        "m_jenis_kelas.nama as is_reguler_nama," +
                        "t_matakuliah_tawar.merdeka_belajar_id," +
                        "m_merdeka_belajar.nama as merdeka_belajar_nama," +
                        "t_matakuliah_tawar.is_kelas_internasional," +
                        "t_matakuliah_tawar.jumlah_kelas " +
                        "FROM t_matakuliah_tawar " +
                        "LEFT JOIN m_matakuliah ON t_matakuliah_tawar.id_matakuliah = m_matakuliah.id " +
                        "LEFT JOIN m_jurusan ON t_matakuliah_tawar.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_tahun_ajar ON t_matakuliah_tawar.id_tahun_ajar = m_tahun_ajar.id " +
                        "LEFT JOIN m_merdeka_belajar ON t_matakuliah_tawar.merdeka_belajar_id = m_merdeka_belajar.id " +
                        "LEFT JOIN m_jenis_kelas ON t_matakuliah_tawar.is_reguler = m_jenis_kelas.id " +
                        "WHERE t_matakuliah_tawar.id_jurusan LIKE ? AND t_matakuliah_tawar.id_tahun_ajar LIKE ?").execute(Tuple.of(id_jurusan, id_tahun_ajar))
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(MatakuliahTawar::from);
    }
}
