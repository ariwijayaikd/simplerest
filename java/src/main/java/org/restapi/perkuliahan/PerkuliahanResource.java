package org.restapi.perkuliahan;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/perkuliahan/matakuliah-tawar")
@Tag(name = "Modul Perkuliahan", description = "Modul ini digunakan untuk mendapatkan data seputar matakuliah")
@Produces(MediaType.APPLICATION_JSON)
public class PerkuliahanResource {

    @Inject
    MySQLPool pool;

    @GET
    @Operation(
            summary = "getMatakuliahTawar",
            description = "endpoint ini akan menampilkan daftar matakuliah tawar berdasarkan id_jurusan dan id_tahun_ajar"
    )
    public Multi<MatakuliahTawar> getMatakuliahTawar(@QueryParam("jurusan") Integer id_jurusan, @QueryParam("tahun-ajar") Integer id_tahun_ajar) {
        return MatakuliahTawar.findMatakuliahTawar(pool, id_jurusan, id_tahun_ajar);
    }

    @GET
    @Operation(
            summary = "getMatakuliahTawarKelas",
            description = "endpoint ini akan menampilkan daftar kelas matakuliah tawar berdasarkan id_matakuliah_tawar"
    )
    @Path("/{id_matakuliah_tawar}/kelas")
    public Multi<MatakuliahTawarKelas> getMatakuliahTawarKelas(@PathParam("id_matakuliah_tawar") Integer id_matakuliah_tawar) {
        return MatakuliahTawarKelas.findMatakuliahTawarKelas(pool, id_matakuliah_tawar);
    }

    @GET
    @Operation(
            summary = "getPesertaMatakuliahTawarKelas",
            description = "endpoint ini akan menampilkan daftar peserta dari kelas matakuliah tawar berdasarkan id_matakuliah_tawar"
    )
    @Path("/{id_matakuliah_tawar}/kelas/{id_matakuliah_tawar_kelas}/peserta")
    public Multi<PesertaMatakuliahKelas> getPesertaMatakuliahTawarKelas(@PathParam("id_matakuliah_tawar") Integer id_matakuliah_tawar, @PathParam("id_matakuliah_tawar_kelas") Integer id_matakuliah_tawar_kelas) {
        return PesertaMatakuliahKelas.findPesertaMatakuliahKelas(pool, id_matakuliah_tawar, id_matakuliah_tawar_kelas);
    }
}
