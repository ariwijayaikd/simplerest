package org.restapi.perkuliahan;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;

public class PesertaMatakuliahKelas {
    public Integer id_mahasiswa;
    public String nim;
    public String nama_mahasiswa;

    public PesertaMatakuliahKelas(
            Integer id_mahasiswa,
            String nim,
            String nama_mahasiswa
    ) {
        this.id_mahasiswa = id_mahasiswa;
        this.nim = nim;
        this.nama_mahasiswa = nama_mahasiswa;
    }

    private static PesertaMatakuliahKelas from(Row row) {
        return new PesertaMatakuliahKelas(
                row.getInteger("id_mahasiswa"),
                row.getString("nim"),
                row.getString("nama_mahasiswa")
        );
    }

    public static Multi<PesertaMatakuliahKelas> findPesertaMatakuliahKelas(MySQLPool pool, Integer id_matakuliah_tawar, Integer id_matakuliah_tawar_kelas) {
        return pool.preparedQuery("SELECT " +
                        "t_krs.id_mahasiswa, " +
                        "m_mahasiswa.nim as nim, " +
                        "m_mahasiswa.nama as nama_mahasiswa " +
                        "FROM " +
                        "t_matakuliah_tawar_kelas " +
                        "LEFT JOIN t_matakuliah_tawar ON " +
                        "t_matakuliah_tawar_kelas.id_matakuliah_tawar = t_matakuliah_tawar.id " +
                        "LEFT JOIN m_matakuliah ON " +
                        "t_matakuliah_tawar.id_matakuliah = m_matakuliah.id " +
                        "LEFT JOIN m_tahun_ajar ON " +
                        "t_matakuliah_tawar.id_tahun_ajar = m_tahun_ajar.id " +
                        "LEFT JOIN t_krs_detail ON " +
                        "t_matakuliah_tawar_kelas.id = t_krs_detail.id_matakuliah_tawar_kelas " +
                        "LEFT JOIN t_krs ON " +
                        "t_krs_detail.id_krs = t_krs.id " +
                        "LEFT JOIN m_mahasiswa ON " +
                        "t_krs.id_mahasiswa = m_mahasiswa.id " +
                        "WHERE " +
                        "t_matakuliah_tawar_kelas.id_matakuliah_tawar LIKE ? " +
                        "AND t_matakuliah_tawar_kelas.id LIKE ?").execute(Tuple.of(id_matakuliah_tawar, id_matakuliah_tawar_kelas))
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(PesertaMatakuliahKelas::from);
    }
}