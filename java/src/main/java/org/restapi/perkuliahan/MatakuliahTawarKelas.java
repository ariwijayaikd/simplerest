package org.restapi.perkuliahan;

import io.smallrye.mutiny.Multi;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "getMatakuliahTawarKelas")
public class MatakuliahTawarKelas {

    public Integer id;
    public Integer id_matakuliah_tawar;
    public String nama_matakuliah;
    public String nama_kelas;
    public Integer kapasitas_ruangan;
    public Integer kapasitas_ruangan_merdeka_belajar;
    public String kode_matakuliah;
    public Integer sks;
    public Integer semester;
    public Integer jam_mulai;
    public Integer jam_selesai;

    public MatakuliahTawarKelas(
            Integer id,
            Integer id_matakuliah_tawar,
            String nama_matakuliah,
            String nama_kelas,
            Integer kapasitas_ruangan,
            Integer kapasitas_ruangan_merdeka_belajar,
            String kode_matakuliah,
            Integer sks,
            Integer semester,
            Integer jam_mulai,
            Integer jam_selesai
    ) {
        this.id = id;
        this.id_matakuliah_tawar = id_matakuliah_tawar;
        this.nama_matakuliah = nama_matakuliah;
        this.nama_kelas = nama_kelas;
        this.kapasitas_ruangan = kapasitas_ruangan;
        this.kapasitas_ruangan_merdeka_belajar = kapasitas_ruangan_merdeka_belajar;
        this.kode_matakuliah = kode_matakuliah;
        this.sks = sks;
        this.semester = semester;
        this.jam_mulai = jam_mulai;
        this.jam_selesai = jam_selesai;
    }

    private static MatakuliahTawarKelas from(Row row) {
        return new MatakuliahTawarKelas(
                row.getInteger("id"),
                row.getInteger("id_matakuliah_tawar"),
                row.getString("nama_matakuliah"),
                row.getString("nama_kelas"),
                row.getInteger("kapasitas_ruangan"),
                row.getInteger("kapasitas_ruangan_merdeka_belajar"),
                row.getString("kode_matakuliah"),
                row.getInteger("sks"),
                row.getInteger("semester"),
                row.getInteger("jam_mulai"),
                row.getInteger("jam_selesai"));
    }

    public static Multi<MatakuliahTawarKelas> findMatakuliahTawarKelas(MySQLPool pool, Integer id_matakuliah_tawar) {
        return pool.preparedQuery("SELECT t_matakuliah_tawar_kelas.id," +
                        "t_matakuliah_tawar.id as id_matakuliah_tawar," +
                        "m_matakuliah.nama_matakuliah as nama_matakuliah," +
                        "t_matakuliah_tawar_kelas.nama_kelas," +
                        "t_matakuliah_tawar_kelas.kapasitas_ruangan," +
                        "t_matakuliah_tawar_kelas.kapasitas_ruangan_merdeka_belajar," +
                        "m_matakuliah.kode_matakuliah," +
                        "m_matakuliah.sks," +
                        "m_matakuliah.semester," +
                        "t_matakuliah_tawar_kelas.jam_mulai," +
                        "t_matakuliah_tawar_kelas.jam_selesai " +
                        "FROM t_matakuliah_tawar_kelas " +
                        "LEFT JOIN t_matakuliah_tawar ON t_matakuliah_tawar_kelas.id_matakuliah_tawar = t_matakuliah_tawar.id " +
                        "LEFT JOIN m_matakuliah ON t_matakuliah_tawar.id_matakuliah = m_matakuliah.id " +
                        "WHERE t_matakuliah_tawar.id LIKE ?").execute(Tuple.of(id_matakuliah_tawar))
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(MatakuliahTawarKelas::from);
    }
}