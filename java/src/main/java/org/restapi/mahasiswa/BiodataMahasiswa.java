package org.restapi.mahasiswa;

import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import java.time.LocalDate;

@Schema(name = "getBiodataMahasiswa")
public class BiodataMahasiswa {

    @Schema(required = true, description = "data berupa id mahasiswa", example = "199***")
    public Long id;
    public Integer id_jurusan;
    public String nama_jurusan;
    public String nama_fakultas;
    public Integer status_keaktifan_terakhir;
    public String nama_status_keaktifan_terakhir;
    public String nim;
    public String nama;
    public String nama_tercetak;
    public String gelar_depan;
    public String gelar_belakang;
    public LocalDate tanggal_lahir;
    public String tanggal_lahir_str;
    public String tempat_lahir;
    public String alamat_asal;
    public String kecamatan;
    public String telepon_alamat_asal;
    public String alamat_tinggal;
    public String telepon_alamat_tinggal;
    public String email;
    public Integer id_agama;
    public String nama_agama;
    public Integer id_jenis_kelamin;
    public String nama_jenis_kelamin;
    public LocalDate tanggal_masuk;
    public Integer id_konsentrasi;
    public String nama_konsentrasi;
    public Integer status_perkawinan;
    public String nama_status_perkawinan;
    public Integer id_golongan_darah;
    public Integer id_jalur_masuk;
    public String nama_jalur_masuk;
    public Float ipk_akhir;
    public Float index_capaian;
    public Integer id_negara_asal;
    public String nama_negara_asal;
    public Integer angkatan;
    public String nama_ayah;
    public String nama_ibu;
    public String website;
    public String no_ktp;
    public Integer lama_studi;
    public String image_foto;
    public String file_ktp;

    public BiodataMahasiswa(
            Long id,
            Integer id_jurusan,
            String nama_jurusan,
            String nama_fakultas,
            Integer status_keaktifan_terakhir,
            String nama_status_keaktifan_terakhir,
            String nim, String nama,
            String nama_tercetak,
            String gelar_depan,
            String gelar_belakang,
            LocalDate tanggal_lahir,
            String tanggal_lahir_str,
            String tempat_lahir,
            String alamat_asal,
            String kecamatan,
            String telepon_alamat_asal,
            String alamat_tinggal,
            String telepon_alamat_tinggal,
            String email,
            Integer id_agama,
            String nama_agama,
            Integer id_jenis_kelamin,
            String nama_jenis_kelamin,
            LocalDate tanggal_masuk,
            Integer id_konsentrasi,
            String nama_konsentrasi,
            Integer status_perkawinan,
            String nama_status_perkawinan,
            Integer id_golongan_darah,
            Integer id_jalur_masuk,
            String nama_jalur_masuk,
            Float ipk_akhir,
            Float index_capaian,
            Integer id_negara_asal,
            String nama_negara_asal,
            Integer angkatan,
            String nama_ayah,
            String nama_ibu,
            String website,
            String no_ktp,
            Integer lama_studi,
            String image_foto,
            String file_ktp
    ) {
        this.id = id;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.nama_fakultas = nama_fakultas;
        this.status_keaktifan_terakhir = status_keaktifan_terakhir;
        this.nama_status_keaktifan_terakhir = nama_status_keaktifan_terakhir;
        this.nim = nim;
        this.nama = nama;
        this.nama_tercetak = nama_tercetak;
        this.gelar_depan = gelar_depan;
        this.gelar_belakang = gelar_belakang;
        this.tanggal_lahir = tanggal_lahir;
        this.tanggal_lahir_str = tanggal_lahir_str;
        this.tempat_lahir = tempat_lahir;
        this.alamat_asal = alamat_asal;
        this.kecamatan = kecamatan;
        this.telepon_alamat_asal = telepon_alamat_asal;
        this.alamat_tinggal = alamat_tinggal;
        this.telepon_alamat_tinggal = telepon_alamat_tinggal;
        this.email = email;
        this.id_agama = id_agama;
        this.nama_agama = nama_agama;
        this.id_jenis_kelamin = id_jenis_kelamin;
        this.nama_jenis_kelamin = nama_jenis_kelamin;
        this.tanggal_masuk = tanggal_masuk;
        this.id_konsentrasi = id_konsentrasi;
        this.nama_konsentrasi = nama_konsentrasi;
        this.status_perkawinan = status_perkawinan;
        this.nama_status_perkawinan = nama_status_perkawinan;
        this.id_golongan_darah = id_golongan_darah;
        this.id_jalur_masuk = id_jalur_masuk;
        this.nama_jalur_masuk = nama_jalur_masuk;
        this.ipk_akhir = ipk_akhir;
        this.index_capaian = index_capaian;
        this.id_negara_asal = id_negara_asal;
        this.nama_negara_asal = nama_negara_asal;
        this.angkatan = angkatan;
        this.nama_ayah = nama_ayah;
        this.nama_ibu = nama_ibu;
        this.website = website;
        this.no_ktp = no_ktp;
        this.lama_studi = lama_studi;
        this.image_foto = image_foto;
        this.file_ktp = file_ktp;
    }

    private static BiodataMahasiswa from(Row row) {
        return new BiodataMahasiswa(
                row.getLong("id"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getString("nama_fakultas"),
                row.getInteger("status_keaktifan_terakhir"),
                row.getString("nama_status_keaktifan_terakhir"),
                row.getString("nim"),
                row.getString("nama"),
                row.getString("nama_tercetak"),
                row.getString("gelar_depan"),
                row.getString("gelar_belakang"),
                row.getLocalDate("tanggal_lahir"),
                row.getString("tanggal_lahir_str"),
                row.getString("tempat_lahir"),
                row.getString("alamat_asal"),
                row.getString("kecamatan"),
                row.getString("telepon_alamat_asal"),
                row.getString("alamat_tinggal"),
                row.getString("telepon_alamat_tinggal"),
                row.getString("email"),
                row.getInteger("id_agama"),
                row.getString("nama_agama"),
                row.getInteger("id_jenis_kelamin"),
                row.getString("nama_jenis_kelamin"),
                row.getLocalDate("tanggal_masuk"),
                row.getInteger("id_konsentrasi"),
                row.getString("nama_konsentrasi"),
                row.getInteger("status_perkawinan"),
                row.getString("nama_status_perkawinan"),
                row.getInteger("id_golongan_darah"),
                row.getInteger("id_jalur_masuk"),
                row.getString("nama_jalur_masuk"),
                row.getFloat("ipk_akhir"),
                row.getFloat("index_capaian"),
                row.getInteger("id_negara_asal"),
                row.getString("nama_negara_asal"),
                row.getInteger("angkatan"),
                row.getString("nama_ayah"),
                row.getString("nama_ibu"),
                row.getString("website"),
                row.getString("no_ktp"),
                row.getInteger("lama_studi"),
                row.getString("image_foto"),
                row.getString("file_ktp"));
    }

    public static Uni<BiodataMahasiswa> findBiodataById(MySQLPool pool, Long id) {
        return pool.preparedQuery("SELECT m_mahasiswa.id," +
                        "m_mahasiswa.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_fakultas.nama_fakultas," +
                        "m_mahasiswa.status_keaktifan_terakhir," +
                        "m_keaktifan.nama as nama_status_keaktifan_terakhir," +
                        "m_mahasiswa.nim," +
                        "m_mahasiswa.nama," +
                        "m_mahasiswa.nama_tercetak," +
                        "m_mahasiswa.gelar_depan," +
                        "m_mahasiswa.gelar_belakang," +
                        "m_mahasiswa.tanggal_lahir," +
                        "m_mahasiswa.tanggal_lahir_str," +
                        "m_mahasiswa.tempat_lahir," +
                        "m_mahasiswa.alamat_asal," +
                        "m_mahasiswa.kecamatan," +
                        "m_mahasiswa.telepon_alamat_asal," +
                        "m_mahasiswa.alamat_tinggal," +
                        "m_mahasiswa.telepon_alamat_tinggal," +
                        "m_mahasiswa.email," +
                        "m_mahasiswa.id_agama," +
                        "m_agama.nama as nama_agama," +
                        "m_mahasiswa.id_jenis_kelamin," +
                        "m_jenis_kelamin.nama as nama_jenis_kelamin," +
                        "m_mahasiswa.tanggal_masuk," +
                        "m_mahasiswa.id_konsentrasi," +
                        "m_konsentrasi.nama_konsentrasi," +
                        "m_mahasiswa.status_perkawinan," +
                        "m_status_perkawinan.nama as nama_status_perkawinan," +
                        "m_mahasiswa.id_golongan_darah," +
                        "m_mahasiswa.id_jalur_masuk," +
                        "m_jalur_masuk.jalur_masuk as nama_jalur_masuk," +
                        "m_mahasiswa.ipk_akhir," +
                        "m_mahasiswa.index_capaian," +
                        "m_mahasiswa.id_negara_asal," +
                        "m_negara.nama as nama_negara_asal," +
                        "m_mahasiswa.angkatan as angkatan,"+
                        "m_mahasiswa.nama_ayah," +
                        "m_mahasiswa.nama_ibu," +
                        "m_mahasiswa.website," +
                        "m_mahasiswa.no_ktp," +
                        "m_mahasiswa.lama_studi," +
                        "m_mahasiswa.image_foto," +
                        "m_mahasiswa.file_ktp " +
                        "FROM m_mahasiswa " +
                        "LEFT JOIN m_jurusan ON m_mahasiswa.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_fakultas ON m_jurusan.id_fakultas = m_fakultas.id " +
                        "LEFT JOIN m_keaktifan ON m_mahasiswa.status_keaktifan_terakhir = m_keaktifan.id " +
                        "LEFT JOIN m_agama ON m_mahasiswa.id_agama = m_agama.id " +
                        "LEFT JOIN m_jenis_kelamin ON m_mahasiswa.id_jenis_kelamin = m_jenis_kelamin.id " +
                        "LEFT JOIN m_konsentrasi ON m_mahasiswa.id_konsentrasi = m_konsentrasi.id " +
                        "LEFT JOIN m_status_perkawinan ON m_mahasiswa.status_perkawinan = m_status_perkawinan.id " +
                        "LEFT JOIN m_jalur_masuk ON m_mahasiswa.id_jalur_masuk = m_jalur_masuk.id " +
                        "LEFT JOIN m_negara ON m_mahasiswa.id_negara_asal = m_negara.id " +
                        "WHERE m_mahasiswa.id LIKE ?").execute(Tuple.of(id))
                .onItem().transform(RowSet::iterator)
                .onItem().transform(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }
}
