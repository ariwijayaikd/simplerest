package org.restapi.mahasiswa;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Schema(name = "getListMahasiswa")
public class ListMahasiswa {
    public Long id;
    public Integer id_jurusan;
    public String nama_jurusan;
    public String nama_fakultas;
    public Integer status_keaktifan_terakhir;
    public String nama_status_keaktifan_terakhir;
    public String nim;
    public String nama;
    public String nama_tercetak;

    public ListMahasiswa(
            Long id,
            Integer id_jurusan,
            String nama_jurusan,
            String nama_fakultas,
            Integer status_keaktifan_terakhir,
            String nama_status_keaktifan_terakhir,
            String nim,
            String nama,
            String nama_tercetak
    ) {
        this.id = id;
        this.id_jurusan = id_jurusan;
        this.nama_jurusan = nama_jurusan;
        this.nama_fakultas = nama_fakultas;
        this.status_keaktifan_terakhir = status_keaktifan_terakhir;
        this.nama_status_keaktifan_terakhir = nama_status_keaktifan_terakhir;
        this.nim = nim;
        this.nama = nama;
        this.nama_tercetak = nama_tercetak;
    }

    private static ListMahasiswa from(Row row) {
        return new ListMahasiswa(
                row.getLong("id"),
                row.getInteger("id_jurusan"),
                row.getString("nama_jurusan"),
                row.getString("nama_fakultas"),
                row.getInteger("status_keaktifan_terakhir"),
                row.getString("nama_status_keaktifan_terakhir"),
                row.getString("nim"),
                row.getString("nama"),
                row.getString("nama_tercetak")
        );
    }

    public static Multi<ListMahasiswa> findAllMahasiswa(MySQLPool pool) {
        return pool.query("SELECT m_mahasiswa.id," +
                        "m_mahasiswa.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_fakultas.nama_fakultas," +
                        "m_mahasiswa.status_keaktifan_terakhir," +
                        "m_keaktifan.nama as nama_status_keaktifan_terakhir," +
                        "m_mahasiswa.nim," +
                        "m_mahasiswa.nama," +
                        "m_mahasiswa.nama_tercetak " +
                        "FROM m_mahasiswa " +
                        "LEFT JOIN m_jurusan ON m_mahasiswa.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_fakultas ON m_jurusan.id_fakultas = m_fakultas.id " +
                        "LEFT JOIN m_keaktifan ON m_mahasiswa.status_keaktifan_terakhir = m_keaktifan.id " +
                        "ORDER BY m_mahasiswa.id DESC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ListMahasiswa::from);
    }

    public static Multi<ListMahasiswa> findMahasiswaByQueryParam(MySQLPool pool, String nama) {
        String namasql = "'%" + nama + "%'";
        return pool.query("SELECT m_mahasiswa.id," +
                        "m_mahasiswa.id_jurusan," +
                        "m_jurusan.nama_jurusan," +
                        "m_fakultas.nama_fakultas," +
                        "m_mahasiswa.status_keaktifan_terakhir," +
                        "m_keaktifan.nama as nama_status_keaktifan_terakhir," +
                        "m_mahasiswa.nim," +
                        "m_mahasiswa.nama," +
                        "m_mahasiswa.nama_tercetak " +
                        "FROM m_mahasiswa " +
                        "LEFT JOIN m_jurusan ON m_mahasiswa.id_jurusan = m_jurusan.id " +
                        "LEFT JOIN m_fakultas ON m_jurusan.id_fakultas = m_fakultas.id " +
                        "LEFT JOIN m_keaktifan ON m_mahasiswa.status_keaktifan_terakhir = m_keaktifan.id " +
                        "WHERE m_mahasiswa.nama LIKE "+namasql+
                        " ORDER BY nim DESC").execute()
                .onItem().transformToMulti(set -> Multi.createFrom().iterable(set))
                .onItem().transform(ListMahasiswa::from);
    }
}