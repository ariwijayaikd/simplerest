package org.restapi.mahasiswa;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.mysqlclient.MySQLPool;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/mahasiswa")
@Tag(name = "Modul Mahasiswa", description = "Modul API ini digunakan untuk mendapatkan data mahasiswa")
@Produces(MediaType.APPLICATION_JSON)
public class MahasiswaResource {

        @Inject
        MySQLPool pool;

        @GET
        @Operation(
                summary = "getListMahasiswa",
                description = "endpoint ini akan menampilkan data mahasiswa dan dapat menggunakan query"
        )
        public Multi<ListMahasiswa> getListMahasiswa(@QueryParam("nama") String nama) {
                if (nama == null) {
                        return ListMahasiswa.findAllMahasiswa(pool);
                } else {
                        return ListMahasiswa.findMahasiswaByQueryParam(pool, nama);
                }
        }

        @GET
        @Path("/{id_mahasiswa}")
        @Operation(
                summary = "getBiodataMahasiswa",
                description = "endpoint ini akan menampilkan data lengkap dari mahasiswa tersebut"
        )
        public Uni<Response> getBiodataMahasiswa(@PathParam("id_mahasiswa") Long id) {
                return BiodataMahasiswa.findBiodataById(pool, id)
                        .onItem().transform(mahasiswa -> mahasiswa != null ? Response.ok(mahasiswa) : Response.status(Response.Status.NOT_FOUND))
                        .onItem().transform(Response.ResponseBuilder::build);
        }
}
